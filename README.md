# Aan Zee PeerJS Signaling server for [aanzee-video-chat-component](https://bitbucket.org/JaapBarnhoorn/aanzee-video-chat-component/)

An implementation of [PeerJs Server](https://github.com/peers/peerjs-server)

# Installation / Usage

Host this NodeJs server and point to it in the Aan Zee Video Chat Component
