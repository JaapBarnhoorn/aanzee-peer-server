var app = require('express')();
var http = require('http').createServer(app);
const { ExpressPeerServer } = require('peer');
const port = process.env.PORT || 3000;

const peerServer = ExpressPeerServer(http, {
  debug: true,
});

app.listen(port, () => {
  console.log(`Peer server is running on port ${ port }`);
});

app.use('/', peerServer);
